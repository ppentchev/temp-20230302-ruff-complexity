"""See if we can get flake8 and ruff to disagree."""
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    import asyncio.subprocess as aprocess

    from typing import Final


class Config:
    """A placeholder."""


async def process_detect_lines(  # flake8 says complexity 10, ruff says 11
    cfg: Config, image: str, proc: aprocess.Process
) -> tuple[bytes | None, list[str]]:
    """Read the lines output by `storpool_variant detect`, see if they look okay."""
    assert proc.stdout is not None  # noqa: S101  # mypy needs this

    first_line = None
    rest = b""
    errors: Final = []
    try:  # pylint: disable=too-many-try-statements
        try:
            first_line = await proc.stdout.readline()
        except Exception as err:  # pylint: disable=broad-except  # noqa: BLE001
            errors.append(f"Could not read the first line: {err}")
        cfg.diag(lambda: f"{image}: first line {first_line!r}")

        if first_line:
            first_line = first_line.rstrip(b"\n")
            # pylint: disable-next=while-used
            while True:
                try:
                    more = await proc.stdout.readline()
                except Exception as err:  # pylint: disable=broad-except  # noqa: BLE001
                    errors.append(f"Could not read a further line: {err}")
                    break
                cfg.diag(lambda: f"{image}: more {more!r}")  # noqa: B023

                if not more:
                    break
                rest += b"\n" + more.rstrip(b"\n")

        if rest:
            assert first_line is not None  # noqa: S101  # mypy needs this
            errors.append(f"More than one line of output: {(first_line + rest)!r}")
    finally:
        res: Final = await proc.wait()
        cfg.diag(lambda: f"{image}: exit code {res!r}")
        if res:
            errors.append(f"Non-zero exit code {res}")

    return first_line, errors
